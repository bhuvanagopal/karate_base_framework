package reusable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.simple.parser.ParseException;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;

public class JSONCompare {

    public String compareJSON(String compResponse) throws JSONException, FileNotFoundException, IOException, ParseException {
        System.out.println("Check complete response "+compResponse);
        String completeResponse = compResponse;
        completeResponse = completeResponse.replace("=", ":");
        completeResponse = completeResponse.replace("sResponse", "\"sResponse\"");
        completeResponse = completeResponse.replace("gResponse", "\"gResponse\"");
        completeResponse = completeResponse.replace("mapping", "\"mapping\"");
        String map = completeResponse.substring(completeResponse.lastIndexOf(':'));
        String newMap = map.substring(1, map.length()-1);
        completeResponse = completeResponse.replace(map, ":\""+newMap+"\"}");
        System.out.println("Final response " + completeResponse);
        String resourceName ="mapping.json";
        InputStream is = JSONComparison.class.getResourceAsStream(resourceName);
        if (is == null) {
            throw new NullPointerException("Cannot find resource file " + resourceName);
        }

        JSONTokener tokener = new JSONTokener(is);
        JSONObject mappingObj = new JSONObject(tokener);
        JSONObject jsonObj = new JSONObject(completeResponse);
        System.out.println("jsonObj JSON is " + jsonObj);
        // retrieve the GQL array from the complete response
        String mappingName = jsonObj.getString("mapping").toString();

        // changing the key attribute names of Solr object to map with the GQL object
        JSONObject mapObj = null;
        //changing the key attribute names of Solr object to map with the GQL object
        Iterator<String> keys = mappingObj.keys();
        while(keys.hasNext()) {
            String key = keys.next();
            if (mappingObj.get(key) instanceof JSONObject) {
                System.out.println("Key: "+key+" Value: "+mappingObj.get(key));
                if(key.toString().equalsIgnoreCase(mappingName)){
                    mapObj = (JSONObject) mappingObj.get(key);
                }
            }
        }

        System.out.print("Map object is: "+mapObj);
        Iterator<String> mapKeys = mapObj.keys();

        while(mapKeys.hasNext()) {
            String key = mapKeys.next();
            System.out.println("Key: "+key+" Value: "+mapObj.get(key));
            completeResponse = completeResponse.replace(key, mapObj.getString(key));
        }
        System.out.println("Updated complete response: "+completeResponse);
        jsonObj = new JSONObject(completeResponse);

        // retrieve the GQL array from the complete response
		JSONArray gqlArray = jsonObj.getJSONObject("gResponse").getJSONObject("data").getJSONObject("campaign")
				.getJSONArray("upcList");
        System.out.println("GQL response array is " + gqlArray);
        // retrieve the Solr array from the complete response
		JSONArray solrArray = jsonObj.getJSONObject("sResponse").getJSONObject("response").getJSONArray("docs");
        System.out.println("Solr response array is " + solrArray);
        // convert the GQL array to json object
        JSONObject gqlObj = new JSONObject();
        try {
            gqlObj.put("data:", gqlArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("GQL JSON object is: " + gqlObj.toString());
        // convert the Solr array to json object
        JSONObject solrObj = new JSONObject();
        try {
            solrObj.put("data:", solrArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("Solr JSON object is: " + solrObj.toString());
        System.out.println("GQL object size is: " + gqlArray.length());
        System.out.println("Solr object size is:" + solrArray.length());
        JSONAssert.assertEquals(gqlObj.toString(), solrObj.toString(), new CustomComparator(JSONCompareMode.LENIENT,
				new Customization("load_date", (o1, o2) -> true), new Customization("advertiserid", (o1, o2) -> true),
				new Customization("new_upc_flag", (o1, o2) -> true), new Customization("size", (o1, o2) -> true),
				new Customization("tot_buyer_cnt", (o1, o2) -> true), new Customization("_version_", (o1, o2) -> true),
				new Customization("id", (o1, o2) -> true), new Customization("campaign_id", (o1, o2) -> true)));
        return "";
    }
}