@hub360
Feature: karate api test script for generating session token

* configure headers = headers

  Background:
    * def afterScenarioCall = read("../hooks/afterScenario.js")
    * configure afterScenario =
            """
	            function() {
	                afterScenarioCall(karate.info);
	            }
            """
    * def afterFeatureCall = read("../hooks/afterFeature.js")
    * configure afterFeature =
            """
            function() {
                afterFeatureCall();
            }
            """

  Scenario: Comparing response for upc list from GQL and Solr endpoint
    Given url baseUrl.GQLEndpoint
    * def payload =  read('../dataFiles/payload.json')
    * def sessionToken = payload.sessionToken
    When request sessionToken
    And method post
    Then status 200
    * def retrievedToken = response.data.beginSession.sessionToken
    Then print 'retrieved token', retrievedToken