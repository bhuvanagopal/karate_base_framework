function fn(name, errorMessage) {

    karate.log('SCENARIO NAME:', name);
    karate.log('SCENARIO ERROR MESSAGE: ' + errorMessage);

    var TC_ids;
    var status = 'failed';
    if (name.contains("TestcaseIds")) {
        var arr = name.replace(" ", "").split("TestcaseIds ");
        var createTP = Java.type('sampletest.features.utils.createBuildinTP');
        var tp = new createTP();

//        if (arr[1].contains(",")) {
            TC_ids = arr[1].split(",");
//        }
//        else {
//            TC_ids = arr[1];
//        }

        karate.log("Testcase Ids :" + TC_ids);

        if(errorMessage == null){
            status = 'passed';
        }

        tp.updateStatus(TC_ids,status);
        karate.log("Test case result updated in Targetprocess +++");
    }
}