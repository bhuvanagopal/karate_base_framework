function afterScenarioCall(info) {
//function afterScenarioCall(info, data) {
//    var sample = data;
//    karate.log('Data from scenario is:',sample);
    karate.log('Completed execution of the scenario:', info.scenarioName);
    karate.log('The scenario executed is of type:', info.scenarioType);
    karate.log('Scenario is part of Feature:', info.featureFileName," present in", info.featureDir);
}