function fn() {
	var env = karate.env; // get system property 'karate.env'
	karate.log('karate.env system property was:', env);
	if (!env) {
		env = 'dev';
	}
	var config = {
		baseURL: 'https://reqres.in/api',
		baseUrl: {
			GQLEndpoint:
			'http://10.165.22.210:31032/graphql',
//			'https://hub360-api.catalina.com/graphql',
			SolrEndpoint: 'http://10.165.19.135:8983/solr'
		},
		headers: {
			Host: 'hub360-api.catalina.com',
			Connection: 'keep-alive',
			Content_Length: 1158,
			Accept: 'application/json, text/plain, */*',
			User_Agent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
			Content_Type: 'application/json',
			Origin: 'https://hub360.catalina.,com',
			Sec_Fetch_Site: 'same-site',
			Sec_Fetch_Mode: 'cors',
			Sec_Fetch_Dest: 'empty',
			Referer: 'https://hub360.catalina.com/login',
			Accept_Encoding: 'gzip, deflate, br',
			Accept_Language: 'en-US,en;q=0.5'
		},
    env: env,
		myVarName: 'someValue'
	}
	if (env == 'demo') {
		GQLEndpoint: 'https://hub360-api.catalina.com/graphql'
	} else if (env == 'e2e') {
	}
	return config;
}